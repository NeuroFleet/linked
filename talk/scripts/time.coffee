# Description:
#   Returns local time in given city.
#   Set countdown date and retreive countdown (number of days remaining).
#
# Dependencies:
#   None
#
# Configuration:
#   HUBOT_WWO_API_KEY
#   HUBOT_WWO_API_URL
#
# Commands:
#   hubot time in <city> - Get current time in city
#
#   hubot uptime - Outputs bot uptime
#
#   countdown set #meetupname# #datestring# e.g. countdown set punerbmeetup 21 Jan 2014
#   countdown [for] #meetupname# e.g. countdown punerbmeetup
#   countdown list 
#   countdown delete #meetupname# e.g. countdown delete seattlerbmeetup
#   countdown clear
#
# Notes
#   Request an WWO API KEY in http://www.worldweatheronline.com/
#   The url is likely to be something like http://api.worldweatheronline.com/free/v2/tz.ashx
#
#   City parameter can be:
#     city
#     city, country
#     ip address
#     latitude and longitude (in decimal)
#
# Author:
#   gtoroap, anildigital
#
module.exports = (robot) ->
  # Get countdown message
  getCountdownMsg = (countdownKey) ->
    now = new Date()
    eventTime = new Date(robot.brain.data.countdown[countdownKey].date)
    gap = eventTime.getTime() - now.getTime()
    gap =  Math.floor(gap / (1000 * 60 * 60 * 24));
    "Only #{gap} days remaining till #{countdownKey}!"

  robot.hear /countdown set (\w+) (.*)/i, (msg) ->
    robot.brain.data.countdown or= {}

    dateString = msg.match[2];

    try 
      date = new Date(dateString);
      if date == "Invalid Date"
        throw "Invalid date passed"
      countdownKey = msg.match[1]

      robot.brain.data.countdown[countdownKey] = {"date" : date.toDateString()} 
      msg.send "Countdown set for #{countdownKey} at #{date.toDateString()}"
      msg.send getCountdownMsg(countdownKey)  if robot.brain.data.countdown.hasOwnProperty(countdownKey)
    catch error
        console.log(error.message)
        msg.send "Invalid date passed!"

  robot.hear /countdown list/i, (msg) ->
    countdowns = robot.brain.data.countdown;
    for countdownKey of countdowns
      msg.send countdownKey + " -> " + new Date(countdowns[countdownKey].date).toDateString() +
        " -> " + getCountdownMsg(countdownKey) if countdowns.hasOwnProperty(countdownKey)

  robot.hear /(countdown)( for)? (.*)/, (msg) ->
    countdownKey = msg.match[3]
    countdowns = robot.brain.data.countdown;
    msg.send getCountdownMsg(countdownKey)  if countdowns.hasOwnProperty(countdownKey)

  robot.hear /countdown clear/i, (msg) ->
    robot.brain.data.countdown = {}
    msg.send "Countdowns cleared"

  robot.hear /countdown delete (.*)/i, (msg) ->
    countdownKey = msg.match[1]
    if robot.brain.data.countdown.hasOwnProperty(countdownKey)
      delete robot.brain.data.countdown[countdownKey]
      msg.send "Countdown for #{countdownKey} deleted."
    else
      msg.send "Countdown for #{countdownKey} does not exist!"

  robot.hear /countdown set$|countdown help/i, (msg) ->
    msg.send "countdown set #meetupname# #datestring# e.g. countdown set PuneRubyMeetup 21 Jan 2014"
    msg.send "countdown [for] #meetupname# e.g. countdown PuneRubyMeetup"
    msg.send "countdown list"
    msg.send "countdown delete #meetupname# e.g. countdown delete HashTagMeetup"
    msg.send "countdown clear"

  start = new Date().getTime()

  robot.respond /uptime/i, (msg) ->
    uptimeMe msg, start, (uptime) ->
      msg.send uptime

  robot.respond /time in (.*)/i, (msg) ->
    unless process.env.HUBOT_WWO_API_KEY
      msg.send 'Please, set HUBOT_WWO_API_KEY environment variable'
      return
    unless process.env.HUBOT_WWO_API_URL
      msg.send 'Please, set HUBOT_WWO_API_URL environment variable'
      return
    msg.http(process.env.HUBOT_WWO_API_URL)
      .query({
        q: msg.match[1]
        key: process.env.HUBOT_WWO_API_KEY
        format: 'json'
      })
      .get() (err, res, body) ->
        try
          result = JSON.parse(body)['data']
          city = result['request'][0]['query']
          currentTime = result['time_zone'][0]['localtime'].slice 11
          msg.send "Current time in #{city} ==> #{currentTime}"
        catch error
          msg.send "Sorry, no city found. Please, check your input and try it again"

numPlural = (num) ->
  if num != 1 then 's' else ''

uptimeMe = (msg, start, cb) ->
  now = new Date().getTime()
  uptime_seconds = Math.floor((now - start) / 1000)
  intervals = {}
  intervals.day = Math.floor(uptime_seconds / 86400)
  intervals.hour = Math.floor((uptime_seconds % 86400) / 3600)
  intervals.minute = Math.floor(((uptime_seconds % 86400) % 3600) / 60)
  intervals.second = ((uptime_seconds % 86400) % 3600) % 60

  elements = []
  for own interval, value of intervals
    if value > 0
      elements.push value + ' ' + interval + numPlural(value)

  if elements.length > 1
    last = elements.pop()
    response = elements.join ', '
    response += ' and ' + last
  else
    response = elements.join ', '

  cb 'I\'ve been sentient for ' + response

