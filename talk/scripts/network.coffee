# Description:
#   Return Hubot's external IP address (via jsonip.com)
#   Uses downforeveryoneorjustme.com to check if a site is up
#
# Dependencies:
#   None
#
# Configuration:
#  None
# 
# Commands:
#   hubot ip - Returns Hubot server's external IP address 
#
#   hubot is <domain> up? - Checks if <domain> is up
#
# Author:
#   ndrake, jmhobbs
     
module.exports = (robot) ->
  robot.respond /ip/i, (msg) ->
    msg.http("http://jsonip.com")
      .get() (err, res, body) ->
        json = JSON.parse(body)
        switch res.statusCode                                
          when 200
            msg.send "External IP address: #{json.ip}"
          else
            msg.send "There was an error getting external IP (status: #{res.statusCode})."

  #*****************************************************************************
                
  robot.respond /is (?:http\:\/\/)?(.*?) (up|down)(\?)?/i, (msg) ->
    isUp msg, msg.match[1], (domain) ->
      msg.send domain

isUp = (msg, domain, cb) ->
  msg.http("http://isitup.org/#{domain}.json")
    .header('User-Agent', 'Hubot')
    .get() (err, res, body) ->
      response = JSON.parse(body)
      if response.status_code is 1
        cb "#{response.domain} looks UP from here."
      else if response.status_code is 2
        cb "#{response.domain} looks DOWN from here."
      else if response.status_code is 3
        cb "Are you sure '#{response.domain}' is a valid domain?"
      else
        msg.send "Not sure, #{response.domain} returned an error."

