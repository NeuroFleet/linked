#!/usr/bin/env python

from gestalt.cmd.generic import *

########################################################################################

@cli.command('list')
@click.option('-f','--field', default=None)
@click.pass_context
def listing(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    if ctx.obj['field'] is None:
        click.echo("Multiverse :")

    for ghost in Ghost.populate():
        if os.path.exists(upath('home', ghost, 'manifest.yml')):
            soul = Ghost.from_env(person=ghost)

            lst = [process(soul, entry) for entry in soul.specs.get('multiverse', [])]

            if ctx.obj['field'] in ('name','link','fqdn'):
                click.echo(' '.join([
                    entry[ctx.obj['field']]
                    for entry in lst
                    if ctx.obj['field'] in entry
                ]))
            else:
                click.echo("\t*) %(alias)s :" % soul)

                for entry in lst:
                    click.echo("")
                    click.echo("\t\t-> name\t: %(name)s" % entry)
                    click.echo("\t\t   link\t: %(link)s" % entry)
                    click.echo("\t\t   fqdn\t: %(fqdn)s" % entry)

#***************************************************************************************

@cli.command('shell')
@click.option('-i', '--interpreter', default='zsh')
@click.pass_context
def shell_cmd(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    if not os.path.exists('/usr/bin/%(interpreter)s' % ctx.obj):
        ctx.obj['interpreter'] = 'bash'

    shell(ctx.obj['interpreter'])

#***************************************************************************************

@cli.command()
@click.pass_context
def status(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    click.echo('Reactor :')
    click.echo('\t-> provider\t: %s' % reactor.provider)
    click.echo('\t-> narrow \t: %s' % reactor.narrow)

########################################################################################

@cli.command()
#@click.option('--count', default=1, help='Number of greetings.')
#@click.option('--skin', prompt=True, default=lambda: os.environ.get('GHOST_SKIN', 'color-admin,control-frog'))
@click.pass_context
def setup(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    click.echo("Python virtual env : %s" % pyenv())

    if not os.path.exists(pyenv()):
        shell('virtualenv', '--distribute', pyenv())

        shell(pyenv('bin', 'easy_install'), '-U', 'pip')

        shell(pyenv('bin', 'pip'), 'install', '-r', bpath('requirements.txt'))

    if os.path.exists(pyenv()):
        ctx.obj['python'] = pyenv('bin','python')
        ctx.obj['pip'] = pyenv('bin','pip')

    reactor.setup(ctx)

########################################################################################

@cli.command()
@click.option('--port', default=8000)
@click.pass_context
def web(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    #click.echo('Serving on http://0.0.0.0:%d/' % port)

    reactor.web(ctx)

#***************************************************************************************

@cli.command()
@click.option('--queue', default='default')
@click.pass_context
def worker(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    click.echo("Running worker on queue '%(queue)s' :" % ctx.obj)

    reactor.worker(ctx)

#***************************************************************************************

@cli.command()
@click.option('--flow', default='default')
@click.pass_context
def scheduler(ctx, *args, **kwargs):
    ctx.obj.update(kwargs)

    click.echo("Running scheduler for flow '%(flow)s' :" % ctx.obj)

    reactor.scheduler(ctx)

########################################################################################

if __name__ == '__main__':
    cli(
        obj={
            'debug': True,
            #'key': 'value',
            'python': '/usr/bin/python',
            'pip':    '/usr/bin/pip',
