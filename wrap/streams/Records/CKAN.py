def scan_ckan(alias):
    from uchikoma.satellite.vortex.models import CKAN_DataHub

    hub = CKAN_DataHub.objects.get(alias=alias)

    data = hub.request('GET', 'action', 'package_list')

    if data['success']:
        print "#) Getting list of CKAN datasets on '%s' :" % hub.realm

        for key in data['result']:
            data = hub.request('GET', 'action', 'package_show?id='+key)

            if data['success']:
                print "\t-> %s ..." % key

                ds,st = hub.datasets.get_or_create(alias=key)

                ds.save()

                for entry in data['result']['resources']:
                    print "\t\t*) %(name)s ..." % entry

                    res,st = ds.resources.get_or_create(id_res=entry['id'])

                    for source,target in {
                        'name':          'alias',
                        'url':           'link',
                        'description':   'summary',

                        'package_id':    'id_pkg',
                        #'id':            'id_res',
                        'revision_id':   'id_rev',

                        'format':        'type_pkg',
                        'resource_type': 'type_res',
                        'url_type':      'type_url',
                    }.iteritems():
                        if source in entry:
                            setattr(res, target, entry[source])

                    res.save()

                    #self.process(hub.Wrapper(self, res['id'], res))

