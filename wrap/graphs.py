from reactor.webapp.shortcuts import *

#*******************************************************************************

from bulbs       import property         as Neo4Prop
from bulbs.utils import current_datetime as Neo4Now

################################################################################

@Reactor.graph.register_node('namespace')
class Namespace(Reactor.graph.Node):
    address   = Neo4Prop.String(nullable=False)
    fullname  = Neo4Prop.String()

    mapping   = Neo4Prop.Dictionary(default={}, nullable=False)

#*******************************************************************************

@Reactor.graph.register_node('metaspace')
class Metaspace(Reactor.graph.Node):
    address   = Neo4Prop.String(nullable=False)
    fullname  = Neo4Prop.String()

    fields    = Neo4Prop.Dictionary(default={}, nullable=False)
    expando   = Neo4Prop.Dictionary(default={}, nullable=False)

#*******************************************************************************

@Reactor.graph.register_node('dataspace')
class Dataspace(Reactor.graph.Node):
    address   = Neo4Prop.String(nullable=False)
    fullname  = Neo4Prop.String()

    sources   = Neo4Prop.Dictionary(default={}, nullable=False)

################################################################################

@Reactor.graph.register_edge('reference')
class ReferencedIn(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)
    triples   = Neo4Prop.Dictionary(default={}, nullable=False)

#*******************************************************************************

@Reactor.graph.register_edge('exists-in')
class ExistsIn(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)
    triples   = Neo4Prop.Dictionary(default={}, nullable=False)

#*******************************************************************************

@Reactor.graph.register_edge('near-by')
class NearBy(Reactor.graph.Edge):
    since     = Neo4Prop.DateTime(default=Neo4Now, nullable=False)
    profile   = Neo4Prop.Dictionary(default={}, nullable=False)

